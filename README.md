# Simple HR management system
I created it for practicing

# Run
to start all services(DB, backend, web) run the next command, in project root folder:
```bash
docker-compose up
```

# Features
 - employees CRUD (create, read, update, delete)
 - maybe auth using Spring Security

# Structure
Project consists of two modules: 
 - backend with Spring Boot
 - web on Angular
